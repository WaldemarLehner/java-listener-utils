package de.waldemarlehner.javaListenerUtils;

public class Tuple
{
    private Tuple(){}

    public static class Two<A,B> {
        private final A a;
        private final B b;

        public Two( A a, B b){
            this.a = a;
            this.b = b;
        }

        public A A()
        {
            return this.a;
        }

        public B B()
        {
            return this.b;
        }
    }

    public static class Three<A,B,C> extends Two<A, B> {


        private final C c;

        public Three( A a, B b, C c )
        {
            super( a, b );
            this.c = c;
        }

        public C C()
        {
            return this.c;
        }
    }

    public static class Four<A,B,C,D> extends Three<A,B,C> {
        private final D d;

        public Four(A a, B b, C c, D d) {
            super(a,b,c);
            this.d = d;
        }

        public D D() {
            return this.d;
        }
    }
}
