package de.waldemarlehner.javaListenerUtils;

/**
 * Interfaces used for exposing the Topic. Listeners can subscribe themselves to the topic, provided they implement this Topic's Listener
 * @param <Listener> The interface / class that a listener has to implement.
 */
public interface ITopic<Listener>
{
    /**
     * Subscribe to a topic. The listener will get triggered when the Topic emits an event.
     * @param listener data consumer that will be triggered when the Topic emits an event.
     */
    void subscribe( Listener listener);

    /**
     * Unsubscribe a Data Consumer
     * @param listener data consumer that will get unsubscribed if it exists in the {@link ITopic}'s internal list.
     */
    void unsubscribe( Listener listener);
}
