package de.waldemarlehner.javaListenerUtils;

/**
 * A generic interface for a Method with a return value. Use {@link Action} if no value should be returned instead.
 */
public class Func
{
    private Func() {}

    @FunctionalInterface
    public interface NoArgs<R> {
        R invoke();
    }

    @FunctionalInterface
    public interface OneArg<A,R> {
        R invoke(A a);
    }

    @FunctionalInterface
    public interface TwoArgs<A, B, R> {
        R invoke(A a, B b);
    }

    @FunctionalInterface
    public interface ThreeArgs<A, B, C, R> {
        R invoke(A a, B b, C c);
    }

    @FunctionalInterface
    public interface FourArgs<A, B, C, D, R> {
        R invoke(A a, B b, C c, D d);
    }

}
