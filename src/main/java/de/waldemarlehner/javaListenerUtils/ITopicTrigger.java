package de.waldemarlehner.javaListenerUtils;

/**
 * Interfaces used to emit an Event
 */
public interface ITopicTrigger<Listener>
{
    /**
     * This method takes an Action that will be executed for all subscribed listeners.
     * @param actionToTake
     */
    void fireListeners(Action.OneArg<Listener> actionToTake);
}
