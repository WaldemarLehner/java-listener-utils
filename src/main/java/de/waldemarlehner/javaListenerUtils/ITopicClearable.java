package de.waldemarlehner.javaListenerUtils;

public interface ITopicClearable<Listener> extends ITopic<Listener>
{
    void clear();
}
