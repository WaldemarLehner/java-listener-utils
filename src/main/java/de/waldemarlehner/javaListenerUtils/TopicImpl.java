package de.waldemarlehner.javaListenerUtils;

import java.util.concurrent.CopyOnWriteArraySet;

public class TopicImpl<Listener> implements ITopic<Listener>, ITopicClearable<Listener>, ITopicTrigger<Listener>
{
    private final CopyOnWriteArraySet<Listener> listeners = new CopyOnWriteArraySet<>();

    public TopicImpl(){}

    @Override
    public void subscribe( Listener listener )
    {
        if (listener == null)
        {
            throw new IllegalArgumentException("Listener has to be not null");
        }
        this.listeners.add(listener);
    }

    @Override
    public void unsubscribe( Listener listener )
    {
        if (listener == null)
        {
            throw new IllegalArgumentException("Listener has to be not null");
        }
        this.listeners.remove(listener);
    }

    @Override
    public void clear()
    {
        this.listeners.clear();
    }

    @Override
    public void fireListeners( Action.OneArg<Listener> actionToTake )
    {
        for (Listener listener : this.listeners)
        {
            actionToTake.invoke(listener);
        }
    }
}
