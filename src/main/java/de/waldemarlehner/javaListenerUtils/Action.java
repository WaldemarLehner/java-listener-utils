package de.waldemarlehner.javaListenerUtils;

/**
 * A generic Interface for a Method that returns void.
 * Use {@link Func} for Methods that do return a value
 */
public class Action
{
    private Action() {}

    @FunctionalInterface
    public interface NoArgs {
        void invoke();
    }

    @FunctionalInterface
    public interface OneArg<A> {
        void invoke(A a);
    }

    @FunctionalInterface
    public interface TwoArgs<A,B> {
        void invoke(A a, B b);
    }

    @FunctionalInterface
    public interface ThreeArgs<A,B,C> {
        void invoke(A a, B b, C c);
    }

    @FunctionalInterface
    public interface FourArgs<A,B,C,D> {
        void invoke(A a, B b, C c, D d);
    }
}
